import React, { useState, useEffect } from 'react'
import {
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import ExploreContainer from '../components/ExploreContainer'
import './Tab3.css'
import {
  DeviceOrientation,
  DeviceOrientationCompassHeading,
} from '@ionic-native/device-orientation'

function useSmoothValue() {
  // eslint-disable-next-line
  const [value, setValue] = useState<null | number>(null)
  let alpha = 0.1
  let beta = 1 - alpha
  function next(newValue: number) {
    setValue(value => (value ? value * beta + newValue * alpha : newValue))
  }
  return { value, next }
}

const Tab3: React.FC = () => {
  const [frequency, setFrequency] = useState(37)
  const [text, setText] = useState('loading...')
  const { value: head, next: setHead } = useSmoothValue()
  const [orientation, setOrientation] =
    useState<null | DeviceOrientationCompassHeading>(null)
  useEffect(() => {
    DeviceOrientation.getCurrentHeading().then(orientation => {
      setText(JSON.stringify(orientation, null, 2))
      setOrientation(orientation)
      setHead(orientation.trueHeading)
    })
    let sub = DeviceOrientation.watchHeading({ frequency }).subscribe(
      orientation => {
        setText(JSON.stringify(orientation, null, 2))
        setOrientation(orientation)
        setHead(orientation.trueHeading)
      },
    )
    return () => {
      sub.unsubscribe()
    }
  }, [frequency, setHead])
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 3</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <pre>
          <code>{text}</code>
        </pre>
        <IonList>
          
          <IonItem>
            <IonLabel>Frequency</IonLabel>
            <IonInput
              type="number"
              value={frequency}
              min="37"
              max="10000"
              onIonChange={e => setFrequency(+e.detail.value!)}
            ></IonInput>
          </IonItem>

          <div
            style={{
              width: '50vw',
              height: '50vw',
              backgroundColor: 'gray',
              transform: `rotate(${head || 0}deg)`,
            }}
          >
            <div
              style={{
                width: '50vw',
                height: '24vw',
                backgroundColor: 'white',
              }}
            ></div>
            <div
              style={{
                width: '48vw',
                height: '2vw',
                backgroundColor: 'black',
                borderLeft: '2vw red solid',
              }}
            ></div>
            <div
              style={{
                width: '50vw',
                height: '24vw',
                backgroundColor: 'white',
              }}
            ></div>
          </div>

          {!orientation ? (
            'loading orientation...'
          ) : (
            <>
              <IonItem>
                <IonLabel>Mag Heading</IonLabel>
                <IonInput value={orientation.magneticHeading}></IonInput>
              </IonItem>

              <IonItem>
                <IonLabel>True Heading</IonLabel>
                <IonInput value={orientation.trueHeading}></IonInput>
              </IonItem>

              <IonItem>
                <IonLabel>Timestamp</IonLabel>
                <IonInput
                  value={new Date(orientation.timestamp).toLocaleTimeString()}
                ></IonInput>
              </IonItem>

              <IonItem>
                <IonLabel>Accuracy </IonLabel>
                <IonInput value={orientation.headingAccuracy}></IonInput>
              </IonItem>
            </>
          )}
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default Tab3
