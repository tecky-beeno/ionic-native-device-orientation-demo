import React, { useState, useEffect } from 'react'
import {
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import ExploreContainer from '../components/ExploreContainer'
import './Tab2.css'
import {
  Gyroscope,
  GyroscopeOrientation,
  GyroscopeOptions,
} from '@ionic-native/gyroscope'

const Tab2: React.FC = () => {
  const [frequency, setFrequency] = useState(1000)
  const [text, setText] = useState('loading...')
  const [orientation, setOrientation] =
    useState<null | GyroscopeOrientation>(null)
  useEffect(() => {
    Gyroscope.getCurrent({ frequency }).then(orientation => {
      setText(JSON.stringify(orientation, null, 2))
      setOrientation(orientation)
    })
    let sub = Gyroscope.watch({ frequency }).subscribe(orientation => {
      setText(JSON.stringify(orientation, null, 2))
      setOrientation(orientation)
    })
    return () => {
      sub.unsubscribe()
    }
  }, [frequency])
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 2</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <pre>
          <code>{text}</code>
        </pre>
        <IonList>
          <IonItem>
            <IonLabel>Frequency</IonLabel>
            <IonInput
              type="number"
              value={frequency}
              min="37"
              max="10000"
              onIonChange={e => setFrequency(+e.detail.value!)}
            ></IonInput>
          </IonItem>

          {!orientation ? (
            'loading orientation...'
          ) : (
            <>
              <IonItem>
                <IonLabel>X</IonLabel>
                <IonInput value={orientation.x}></IonInput>
              </IonItem>

              <IonItem>
                <IonLabel>Y</IonLabel>
                <IonInput value={orientation.y}></IonInput>
              </IonItem>

              <IonItem>
                <IonLabel>Z</IonLabel>
                <IonInput value={orientation.z}></IonInput>
              </IonItem>

              <IonItem>
                <IonLabel>Timestamp</IonLabel>
                <IonInput
                  value={new Date(orientation.timestamp).toLocaleTimeString()}
                ></IonInput>
              </IonItem>
            </>
          )}
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default Tab2
